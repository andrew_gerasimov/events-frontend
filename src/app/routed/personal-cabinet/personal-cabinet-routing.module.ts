import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PersonalCabinetPage} from './pages/personal-cabinet/personal-cabinet.page';
import {DetailEventPage} from "../detail-event/pages/detail-event/detail-event.page";
import {EventResolver} from "../../features/resolver/event.resolver";
import {AnotherUserComponent} from "./components/another-user/another-user.component";
import {UserResolver} from "../../features/resolver/user.resolver";
import {AuthGuards} from "../../auth.guards";
import {PhotoPersonalCabinetComponent} from "./components/photo-personal-cabinet/photo-personal-cabinet.component";
import {PayPalComponent} from "./components/pay-pal/pay-pal.component";

const routes: Routes = [
  {
    path: '',
    component: PersonalCabinetPage,
    canActivate: [AuthGuards],
    children: [{
      outlet: 'primary',
      path: 'photo',
      component: PhotoPersonalCabinetComponent,
      },
      {
        path: "premium",
        component: PayPalComponent,
        outlet : 'prem'
      }]
  },
  {
    path: ':id',
    component: AnotherUserComponent,
    resolve : {
      event: UserResolver
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonalCabinetRoutingModule { }
