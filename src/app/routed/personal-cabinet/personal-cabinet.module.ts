import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddPersonalInformationUserComponent } from './components/add-personal-information-user/add-personal-information-user.component';
import {PersonalCabinetPage} from "./pages/personal-cabinet/personal-cabinet.page";
import {PersonalCabinetRoutingModule} from "./personal-cabinet-routing.module";
import {MatIconModule} from "@angular/material/icon";
import {CloudinaryModule} from "@cloudinary/angular-5.x";
import {FileModule} from "../../features/cloudinary/file.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {ValidationModule} from "../../features/validators/validation.module";
import {MatOptionModule} from "@angular/material/core";
import {MatSelectModule} from '@angular/material/select';
import {ScrollingModule } from '@angular/cdk/scrolling';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatChipsModule} from "@angular/material/chips";
import {MatDividerModule} from "@angular/material/divider";
import { MatCheckboxModule } from '@angular/material/checkbox';
import {CategoriesEventModule} from "../../features/categories-event/categories-event.module";
import {MatButtonModule} from "@angular/material/button";
import { AnotherUserComponent } from './components/another-user/another-user.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {PhotoModule} from "../../features/photo/photo.module";
import {ImageGalleryModule} from "../../features/image-gallery/image-gallery.module";
import { PayPalComponent } from './components/pay-pal/pay-pal.component';
import { PhotoPersonalCabinetComponent } from './components/photo-personal-cabinet/photo-personal-cabinet.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";

@NgModule({
  declarations: [AddPersonalInformationUserComponent, PersonalCabinetPage, AnotherUserComponent, PayPalComponent, PhotoPersonalCabinetComponent],
    imports: [
        CommonModule,
        PersonalCabinetRoutingModule,
        MatIconModule,
        CloudinaryModule,
        FileModule,
        ReactiveFormsModule,
        MatInputModule,
        FormsModule,
        ValidationModule,
        MatOptionModule,
        MatSelectModule,
        ScrollingModule,
        MatAutocompleteModule,
        MatChipsModule,
        MatDividerModule,
        MatCheckboxModule,
        CategoriesEventModule,
        MatButtonModule,
        MatFormFieldModule,
        PhotoModule,
        ImageGalleryModule,
        MatProgressSpinnerModule
    ]
})
export class PersonalCabinetModule { }
