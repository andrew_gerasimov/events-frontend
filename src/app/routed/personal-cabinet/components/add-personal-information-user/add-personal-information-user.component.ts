import { AfterViewChecked, AfterViewInit, Component, DoCheck, forwardRef, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { CurrentUserImpl, CurrentUserService } from '../../../../core/auth/current-user.service';
import { FormBuilder, FormControl, FormGroup, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { nameRegExp } from '../../../../features/validators/directives/name-validator.directive';
import { phoneRegExp } from '../../../../features/validators/directives/phone-validator.directive';
import { emailRegExp } from '../../../../features/validators/directives/email-validator.directive';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { CountryModel } from '../../../../features/other-model/country.model';
import { EventCategory } from '../../../../features/other-model/category.model';
import { CommonService } from '../../../../features/form/services/common.service';
import { UserService } from '../../../../features/users/services/users.service';
import { CityModel } from '../../../../features/other-model/city.model';
import { async } from 'rxjs/internal/scheduler/async';
import { RefreshUserService } from '../../../../core/auth/refresh-user.service';
import {MatDialog} from "@angular/material/dialog";
import {DialogComponent} from "../../../dialog/dialog/dialog.component";

interface FormData {
  chipsControl: string[];
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  country: CountryModel;
  city: CityModel;
  informationAboutYourself: string;
}

export interface User {
  countryName: string;
}

@UntilDestroy()
@Component({
  selector: 'app-add-personal-information-user',
  templateUrl: './add-personal-information-user.component.html',
  styleUrls: ['./add-personal-information-user.component.css']
})
export class AddPersonalInformationUserComponent implements OnInit, DoCheck, AfterViewInit {
  user: CurrentUserImpl;
  formPersonalCabinet: FormGroup;
  countries: CountryModel[];
  cities: CityModel[];
  categoryIs = false;
  numberOfChanges = 0;
  defaultLengthControlCity: number;
  loading: boolean = false;


  categories: EventCategory[] | undefined;

  options: string[] = [];

  categoryUser: string[] = [];

  chipsControlValue$: Observable<any>;

  disabledControl = new FormControl(true);

  filteredOptions: Observable<CountryModel[]>;
  filteredOptionsCity: Observable<CityModel[]>;

  constructor(
    private http: HttpClient,
    private readonly fb: FormBuilder,
    public readonly router: Router,
    private readonly userService: UserService,
    private readonly currentUserService: CurrentUserService,
    private readonly commonService: CommonService,
    private readonly getUserService: RefreshUserService,
    private readonly dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.currentUserService.user$.subscribe((value: CurrentUserImpl) => {
      this.user = value;
      console.log('value for user set');
    });
    this.commonService.getCountries().subscribe((response) => {
      this.countries = response;
      this.filteredOptions = this.formPersonalCabinet.get('country').valueChanges.pipe(
        startWith(''),
        map((value) => (typeof value === 'string' ? value : value.countryName)),
        map((countryName) => (countryName ? this._filter(countryName) : this.countries.slice()))
      );
    });

    console.log(this.user);

    this.formPersonalCabinet = this.initForm();
    this._updateOrGetCity(null);

    this.commonService.getCategories().subscribe((response) => {
      this.categories = response;
      this.chipsControlValue$ = this.formPersonalCabinet.get('chipsControl').valueChanges;
      console.log('chipsControlValue$');
      this.loading = false
    });

    this.disabledControl.valueChanges.pipe(untilDestroyed(this)).subscribe((val) => {
      if (val) {
        this.formPersonalCabinet.get('chipsControl').enable();
      } else {
        this.formPersonalCabinet.get('chipsControl').disable();
      }
    });

    this.formPersonalCabinet.get('country').valueChanges.subscribe(() => this._updateOrGetCity());
  }

  /**
   * Необходимо сначала получить значения, а потом уже проинициализировать компонент
   */
  ngDoCheck(): void {
    if (!this.categoryIs) {
      for (const category of this.categories) {
        this.options.push(category.nameCategory);
      }
      console.log(this.options);
      this.categoryIs = true;
    }
  }

  private initForm(): FormGroup {
    let cityDef = this.user.cityDTO ? this.user.cityDTO.titleRu : '';
    const catDef = this.getDefCategoriesUser();
    this.defaultLengthControlCity = cityDef.length;
    return this.fb.group({
      firstName: this.fb.control(this.user.firstName, [Validators.required, nameRegExp]),
      lastName: this.fb.control(this.user.lastName, [Validators.required, nameRegExp]),
      username: this.fb.control(this.user.username, [Validators.required]),
      email: this.fb.control(this.user.email, [Validators.required, emailRegExp]),
      country: this.fb.control(this.user.country.countryName, [Validators.required]),
      city: this.fb.control(cityDef),
      informationAboutYourself: this.fb.control(this.user.informationAboutYourself),
      chipsControl: this.fb.control(catDef)
    });
  }

  SubmitInformation(value: FormData): void {
    if (value.country.countryUuidId === undefined) {
      value.country = this.user.country;
    }
    if (value.city.cityUuidId === undefined) {
      value.city = this.user.cityDTO;
    }
    console.log(value.city);
    this.userService
      .updateUser(
        value.firstName,
        value.lastName,
        value.email,
        value.country,
        value.informationAboutYourself,
        value.username,
        value.city,
        value.chipsControl
      )
      .subscribe(
        () => {
          console.log('user info update success');
        },
        (error: HttpErrorResponse) => {
          console.log('user info update error');
          console.error('Error', error);
        }
      );

    const chipsValue = value.chipsControl;
    console.log(chipsValue);
    this.userService.patchCategories(chipsValue).subscribe(
      () => {
        console.log('categories update success');
        this.updateUser();
      },
      (error: HttpErrorResponse) => {
        console.log('categories update error');
        console.error('Error', error);
      }
    );
  }

  displayFn(country: CountryModel): string {
    return country && country.countryName ? country.countryName : '';
  }

  displayFnCity(city: CityModel): string {
    return city && city.titleRu ? city.titleRu : '';
  }

  private _filter(name: string): CountryModel[] {
    const filterValue = name.toLowerCase();
    return this.countries.filter((option) => option.countryName.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filterCity(name: string): CityModel[] {
    const filterValue = name.toLowerCase();
    return this.cities.filter((option) => option.titleRu.toLowerCase().indexOf(filterValue) === 0);
  }

  private _updateOrGetCity(value?: string): void {
    if (value === '') {
      value = null;
    }
    let country = this.user.country.countryUuidId;
    //пытаемя поменять предложенные города в зависимости от страны. Работает лишь в том случае,
    //мы выбрали другую страну из предложенного варианта.
    try {
      console.log('country' + this.formPersonalCabinet.get('country').value.countryUuidId);
      if (this.formPersonalCabinet.get('country').value.countryUuidId !== undefined)
        country = this.formPersonalCabinet.get('country').value.countryUuidId;
    } catch (e) {
      console.log(e);
    }
    this.commonService.getCities(country, value).subscribe((response) => {
      this.cities = response;
      this.filteredOptionsCity = this.formPersonalCabinet.get('city').valueChanges.pipe(
        startWith(''),
        map((value) => (typeof value === 'string' ? value : value.titleRu)),
        map((titleRu) => (titleRu ? this._filterCity(titleRu) : this.cities.slice()))
      );
    });
  }

  ngAfterViewInit(): void {
    this.formPersonalCabinet.get('city').valueChanges.subscribe(() => {
      if (
        this.numberOfChanges > 2 &&
        this.defaultLengthControlCity < this.formPersonalCabinet.get('city').value.length
      ) {
        this._updateOrGetCity(this.formPersonalCabinet.get('city').value);
      }
      this.defaultLengthControlCity = this.formPersonalCabinet.get('city').value.length;
      this.numberOfChanges++;
    });
  }

  private getDefCategoriesUser(): string[] | string {
    let cat = [];
    if (this.user.categories !== undefined) {
      for (let category of this.user.categories) {
        cat.push(category.nameCategory);
      }
      return cat;
    } else return '';
  }

  private updateUser() {
    this.getUserService.refreshCurrentUser().toPromise();
  }

  openDialog() {
    this.dialog.open(DialogComponent);
  }
}
