import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DefaultOrganizerService} from "../../../../features/users/services/defaultOrganizer.service";
import {EventsService} from "../../../../features/events/services/events.service";
import {CurrentUserImpl, CurrentUserService} from "../../../../core/auth/current-user.service";
import {AnotherUser} from "../../../../features/users/models/another-user.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {nameRegExp} from "../../../../features/validators/directives/name-validator.directive";
import {emailRegExp} from "../../../../features/validators/directives/email-validator.directive";

@Component({
  selector: 'app-another-user',
  templateUrl: './another-user.component.html',
  styleUrls: ['./another-user.component.css']
})
export class AnotherUserComponent implements OnInit {

  user: AnotherUser;
  formPersonalCabinet: FormGroup;
  options: string[] = [];
  lookPhoto = true
  userCurrent: CurrentUserImpl;



  constructor(
    private readonly route: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly currentUserService: CurrentUserService
  ) { }

  ngOnInit(): void {
    this.currentUserService.user$.subscribe((value: CurrentUserImpl) => {
      this.userCurrent = value;
    });
    this.route.data.subscribe(data => {
      this.user = data.event
      if (this.user.userId === this.userCurrent.userId){
        this.router.navigate(["/personal-cabinet"])
      } 
    });
    this.formPersonalCabinet = this.initForm();
  }

  private initForm(): FormGroup {
    let cityDef = this.user.cityDTO ? this.user.cityDTO.titleRu : '';
    const catDef = this.getDefCategoriesUser();
    return this.fb.group({
      firstName: this.fb.control(this.user.firstName, [Validators.required, nameRegExp]),
      lastName: this.fb.control(this.user.lastName, [Validators.required, nameRegExp]),
      username: this.fb.control(this.user.username, [Validators.required]),
      email: this.fb.control(this.user.email, [Validators.required, emailRegExp]),
      country: this.fb.control(this.user.country.countryName, [Validators.required]),
      city: this.fb.control(cityDef),
      informationAboutYourself: this.fb.control(this.user.informationAboutYourself),
      chipsControl: this.fb.control(catDef)
    });
  }

  private getDefCategoriesUser(): string[] | string{
    let cat = [];
    if (this.user.categories !== undefined){
      for (let category of this.user.categories) {
        cat.push(category.nameCategory)
      }
      return cat
    }
    else
      return ''
  }

  public disable(): void{
    this.lookPhoto=!this.lookPhoto;
  }


}
