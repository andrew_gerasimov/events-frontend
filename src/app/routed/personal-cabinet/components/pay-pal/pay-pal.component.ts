import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UserService} from "../../../../features/users/services/users.service";
import {PayInformationModel} from "../../../../features/other-model/payInformation.model";
import {CurrentUserImpl, CurrentUserService} from "../../../../core/auth/current-user.service";

declare var paypal;

@Component({
  selector: 'app-pay-pal',
  templateUrl: './pay-pal.component.html',
  styleUrls: ['./pay-pal.component.css']
})
export class PayPalComponent implements OnInit {

  @ViewChild('paypalRef', {static: true}) paypalElement: ElementRef;
  constructor(
    private readonly userService: UserService,
    private readonly currentUserService: CurrentUserService,
  ) {
    this.currentUserService.user$.subscribe((value: CurrentUserImpl) => {
      this.user = value;
      this.paidFor = this.user.prem;
    });
  }
  user: CurrentUserImpl;
  paidFor:boolean;
  result: PayInformationModel

  ngOnInit() {
    paypal
      .Buttons({
        style: {
          color: 'gold',
          shape: 'rect',
          size: 'medium'
        },
        createOrder: (data, actions) => {
          data = data
          return actions.order.create({
            purchase_units: [
              {
                amount: {
                  currency_code: 'RUB',
                  value: 999
                }
              }
            ]
          });
        },
        onApprove: async (data, actions) => {
          data = data
          const order = await actions.order.capture();
          this.paidFor = true;
          console.log(order);
          console.log(data)
          this.result = order;
          this.userService.postPremium(this.result.create_time,
            this.result.status,
            this.result.payer.email_address,
            this.result.purchase_units[0].amount.value + this.result.purchase_units[0].amount.currency_code
          ).subscribe(() => window.location.reload());
        },
        onError: err => {
          console.log(err);
        }
      })
      .render(this.paypalElement.nativeElement);
  }

}
