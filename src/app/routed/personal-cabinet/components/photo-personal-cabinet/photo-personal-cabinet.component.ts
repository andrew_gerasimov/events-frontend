import { Component, OnInit } from '@angular/core';
import {CurrentUserImpl, CurrentUserService} from "../../../../core/auth/current-user.service";

@Component({
  selector: 'app-photo-personal-cabinet',
  templateUrl: './photo-personal-cabinet.component.html',
  styleUrls: ['./photo-personal-cabinet.component.css']
})
export class PhotoPersonalCabinetComponent implements OnInit {
  user: CurrentUserImpl;

  constructor(
    private readonly currentUserService: CurrentUserService,
  ) { }

  ngOnInit(): void {
    this.currentUserService.user$.subscribe((value: CurrentUserImpl) => {
      this.user = value;
    });
  }

}
