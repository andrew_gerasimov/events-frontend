import {AfterContentChecked, Component, Input, OnInit} from '@angular/core';
import { MinimalInformationUser } from '../../../../features/users/models/minimal-information-user.model';
import { ChatMessageInterfaces } from '../../models/chatMessage.interfaces';
import { ChatService } from '../../service/chat.service';
import { CurrentUserImpl, CurrentUserService } from '../../../../core/auth/current-user.service';
import { ChatRoomInterfaces } from '../../models/chatRoom.interfaces';
import { UserService } from '../../../../features/users/services/users.service';
import { Router } from '@angular/router';
import {UntilDestroy} from "@ngneat/until-destroy";

@UntilDestroy()
@Component({
  selector: 'app-chat-user-minimal-information',
  templateUrl: './chat-user-minimal-information.component.html',
  styleUrls: ['./chat-user-minimal-information.component.css']
})
export class ChatUserMinimalInformationComponent implements OnInit {
  userChat: MinimalInformationUser;
  @Input()
  chatRoom: ChatRoomInterfaces;
  currentDate;
  countNewMessage: number = 0;
  @Input()
  update: boolean
  user: CurrentUserImpl;
  lastMessage: ChatMessageInterfaces;
  isDataAvailable:boolean = false;
  loading: boolean = false;

  constructor(
    private readonly chatService: ChatService,
    private readonly currentUserService: CurrentUserService,
    private readonly userService: UserService,
    public readonly router: Router
  ) {}

  ngOnInit(): void {
    this.isDataAvailable = true
    this.loading = true
    this.currentUserService.user$.subscribe((value: CurrentUserImpl) => {
      this.user = value;
    });
    this.currentDate = new Date()

    this.userService
      .getMinimalInformationUserById(this.chatRoom.senderId)
      .subscribe((userChat) => {
        this.userChat = userChat
        console.log(this.userChat)
        this.isDataAvailable = false
      });

    this.chatService.getChatLastMessage(this.chatRoom.chatId).subscribe(
      (msg) => {
        this.lastMessage = msg;
        this.loading = false
      },
      () => {
        this.loading = false
        this.lastMessage = null
      }
    );

    this.chatService.countNewMessages(this.chatRoom.senderId).subscribe(
      count => this.countNewMessage=count
    );

  }

  ngAfterContentChecked(){
    if (this.chatRoom.update === true) {
      this.chatService.getChatLastMessage(this.chatRoom.chatId).subscribe(
        (msg) => {
          this.lastMessage = msg;
        },
        () => (this.lastMessage = null)
      );
      this.chatService.countNewMessages(this.chatRoom.senderId).subscribe(
        count => this.countNewMessage=count
      );
      this.chatRoom.update = false
    }
  }

}
