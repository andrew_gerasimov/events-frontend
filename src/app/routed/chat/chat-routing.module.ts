import {RouterModule, Routes} from "@angular/router";
import {PersonalCabinetPage} from "../personal-cabinet/pages/personal-cabinet/personal-cabinet.page";
import {NgModule} from "@angular/core";
import {ChatPage} from "./pages/ chat/chat.page";
import {AnotherUserComponent} from "../personal-cabinet/components/another-user/another-user.component";
import {UserResolver} from "../../features/resolver/user.resolver";
import {ChatRoomResolver} from "../../features/resolver/chatRoom.resolver";

const routes: Routes = [
  {
    path: '',
    component: ChatPage,
  },
  {
    path: ':id',
    component: ChatPage,
    resolve : {
      event: ChatRoomResolver,
      user: UserResolver
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatRoutingModule{ }
