export interface ChatMessageInterfaces{
  id?: string;
  chatId?: string;
  senderId: string;
  recipientId: string;
  content: string;
  timestamp: string;
  status?: string;
}
