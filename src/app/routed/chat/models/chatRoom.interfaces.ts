export interface ChatRoomInterfaces {
  id: string;
  chatId : string;
  senderId: string;
  recipientId: string;
  update?:boolean
}
