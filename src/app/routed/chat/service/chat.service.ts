import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";
import {ChatMessageInterfaces} from "../models/chatMessage.interfaces";
import {ChatRoomInterfaces} from "../models/chatRoom.interfaces";

@Injectable({
  providedIn: 'root'
})
export class ChatService{
  constructor(private readonly http: HttpClient){}

  countNewMessages(recipient_id): Observable<number> {
    return this.http.get<number>(`${environment.chat}/messages/${localStorage.getItem('user_id')}/${recipient_id}/count`);
  }

  countAllNewMessages(): Observable<number> {
    return this.http.get<number>(`${environment.chat}/messages/${localStorage.getItem('user_id')}/count`);
  }

  getAllChatRoom(): Observable<ChatRoomInterfaces[]>{
    return this.http.get<ChatRoomInterfaces[]>(`${environment.chat}/chatRoom/${localStorage.getItem('user_id')}`);
  }

  getChatMessages(recipient_id: string): Observable<ChatMessageInterfaces[]>{
    return this.http.get<ChatMessageInterfaces[]>(`${environment.chat}/messages/${localStorage.getItem('user_id')}/${recipient_id}`);
  }

  getChatMessagesLastByRecipientId(recipient_id: string): Observable<ChatMessageInterfaces>{
    return this.http.get<ChatMessageInterfaces>(`${environment.chat}/messages/${localStorage.getItem('user_id')}/${recipient_id}/last`);
  }

  getChatLastMessage(chat_id: string): Observable<ChatMessageInterfaces>{
    return this.http.get<ChatMessageInterfaces>(`${environment.chat}/chatRoom/${chat_id}/last`);
  }

}
