import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ChatPage} from "./pages/ chat/chat.page";
import {ChatRoutingModule} from "./chat-routing.module";
import {WebsocketModule} from "../../core/chat/websocket.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ChatUserMinimalInformationComponent } from './components/chat-user-minimal-information/chat-user-minimal-information.component';
import {PhotoModule} from "../../features/photo/photo.module";
import {MatFormFieldModule} from "@angular/material/form-field";
import {TextFieldModule} from "@angular/cdk/text-field";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {ScrollingModule} from "@angular/cdk/scrolling";
import {PhotoPersonChatModule} from "../../features/photo-person-chat/photo-person-chat.module";
import {CloudinaryModule} from "@cloudinary/angular-5.x";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";



@NgModule({
  declarations: [ChatPage, ChatUserMinimalInformationComponent],
    imports: [
        CommonModule,
        ChatRoutingModule,
        FormsModule,
        PhotoModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        TextFieldModule,
        MatInputModule,
        MatIconModule,
        ScrollingModule,
        PhotoPersonChatModule,
        CloudinaryModule,
        MatProgressSpinnerModule,
    ]
    ,
})
export class ChatModule { }
