import {AfterViewChecked, AfterViewInit, ChangeDetectionStrategy, Component, OnInit, ViewChild} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {ChatService} from "../../service/chat.service";
import * as SockJS from "sockjs-client";
import {Stomp} from "@stomp/stompjs";
import {CurrentUserImpl, CurrentUserService} from "../../../../core/auth/current-user.service";
import {MinimalInformationUser} from "../../../../features/users/models/minimal-information-user.model";
import {UserService} from "../../../../features/users/services/users.service";
import {ChatRoomInterfaces} from "../../models/chatRoom.interfaces";
import {ActivatedRoute, Router} from "@angular/router";
import {ChatMessageInterfaces} from "../../models/chatMessage.interfaces";
import {FormControl, FormGroup} from "@angular/forms";
import {CommentEvent} from "../../../../features/other-model/comment.model";
import {CdkVirtualScrollViewport} from "@angular/cdk/scrolling";
import {UntilDestroy} from "@ngneat/until-destroy";

@UntilDestroy()
@Component({
  templateUrl: './сhat.page.html',
  styleUrls: ['./chat.page.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatPage implements OnInit, AfterViewChecked{
  user: CurrentUserImpl;
  @ViewChild("messages") viewport: CdkVirtualScrollViewport;
  chatRooms: ChatRoomInterfaces[] = []
  chatMessages: ChatMessageInterfaces[]
  userChat: MinimalInformationUser
  form: FormGroup;
  anotherUserId;
  numbers = [];
  photoAnotherUser: string
  contentCheck = 0
  updateChatId;
  updateChat;
  loading: boolean = false;



  private stompClient = null;

  constructor(
    private readonly title: Title,
    private readonly chatService: ChatService,
    private readonly currentUserService: CurrentUserService,
    private readonly userService: UserService,
    private readonly route: ActivatedRoute,
    public readonly router: Router
  ) {
    this.title.setTitle('Чат');
  }

  ngOnInit(): void {
    this.loading = true;
    this.currentUserService.user$.subscribe((value: CurrentUserImpl) => {
      this.user = value;
    });
    this.updateChatRoom()
    const socket = new SockJS('https://chat-backend.xyz/ws');
    this.stompClient = Stomp.over(socket);
    const _this = this;

    this.route.data.subscribe((data) => {
      this.loading = true;
      this.chatMessages = data.event;
      this.userChat = null
      this.photoAnotherUser = null
      this.anotherUserId = this.router.url.substr(6)
      if (this.anotherUserId.length!==0) {
        this.userService.getMinimalInformationUserById(_this.anotherUserId).subscribe(data => {
            this.userChat = data
            this.photoAnotherUser = this.userChat.medias[0].mediaUrl;
            this._scrollToBottom()
            this.loading = false;
          }
        )
      }
    });

    this.stompClient.connect({}, function (frame) {
      console.log('Connected: ' + frame);
      //подписка текущего пользователя на оповещения от других пользователей
      _this.stompClient.subscribe(`/user/`+ _this.user.userId +`/queue/messages`, function (hello) {
        //получаем ответ
        console.log("Получаем ответ")
        const notification = JSON.parse(hello.body);
        console.log(notification.senderId)
        _this.updateChatRoomByNotification(notification.senderId)
        _this.updateChatMessage(notification.senderId)
      });
    });
    this.form = new FormGroup({
      textMessage: new FormControl()
    });
  }


  disconnect() {
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }
  }

  //появление нового чата без обновления страницы
  updateChatRoomByNotification(senderId: string){
    this.chatService.getAllChatRoom().subscribe(rooms => {
      //если не новый чат, то не будет обновлять чаты
      if (this.chatRooms.length !== rooms.length){
        for (let room of rooms) {
          if (room.senderId === senderId) {
            this.chatRooms = [...this.chatRooms, room]
            break
          }
        }
      }else {
        for (let room of rooms) {
          if (room.senderId === senderId) {
            this.updateChatId = room.chatId
            this.updateChat = room;
            break
          }
        }
        for (let i = 0; i < this.chatRooms.length; i++) {
          if (this.chatRooms[i].chatId === this.updateChatId){
            this.chatRooms[i].update = true;
          }
          else {
            this.chatRooms[i].update = false;
          }
        }
      }
    })
  }

  //загружаем инофрмацию о наших чат комнатах
  updateChatRoom(){
    this.chatRooms = []
    this.chatService.getAllChatRoom().subscribe(rooms => {
      for (let room of rooms) {
        this.chatRooms.push(room)
      }
    })
  }


  updateChatMessage(senderId: string){
    console.log('текущий роутер'  + this.router.url)
    console.log('Общаемся с пользователем ' + this.userChat.userId)
    if (this.router.url === `/chat/${this.userChat.userId}` && this.userChat.userId === senderId){
      console.log('пришло сообщение от другого пользователя, а мы как раз с ним чатимся')
      this.chatService.getChatMessagesLastByRecipientId(this.userChat.userId).subscribe((data)=>{
        console.log('получаем последнее сообщение')
        console.log(data)
        //вставляем последнее сообщение в наш текущий чат
        this.chatMessages = [...this.chatMessages, data];
        this._scrollToBottom()
      })
    }
  }

  sendMessage(){
    if (this.form.get('textMessage').value.trim()!=='') {
      //формируем сообщение
      const message = {
        content: this.form.get('textMessage').value,
        senderId: this.user.userId,
        recipientId: this.router.url.substr(6),
        timestamp: new Date(),
      };
      console.log("отправка сообщения")
      this.stompClient.send(
        '/app/chat',
        {},
        JSON.stringify(message)
      );
      //заменить на получение нового из базы
      const sendMessage = {
        senderId: this.user.userId,
        recipientId: this.router.url.substr(6),
        content: this.form.get('textMessage').value,
        timestamp: (new Date()).toString()
      }
      if (this.chatMessages.length === 0)
        this.chatMessages = [sendMessage]
      else {
        this.chatMessages = [...this.chatMessages, sendMessage]
      }
      //прокрутка вниз
      this._scrollToBottom()
      //поменять на только нашу комнату
      this.updateChatRoomByNotification(this.router.url.substr(6))
      //сбрасываем форму
      this.form.reset();
    }
  }

  //прокрутка страницы вниз в момент загрузки
  //хорошие костыли
  ngAfterViewChecked() {
    if (this.contentCheck < 20) {
      this._scrollToBottom()
      this.contentCheck++;
    }
  }

  ngOnDestroy(){
  }

  private _scrollToBottom() {
      setTimeout(() => {
        this.viewport.scrollTo({
          bottom: 0,
          behavior: 'auto',
        });
      }, 0);
      setTimeout(() => {
        this.viewport.scrollTo({
          bottom: 0,
          behavior: 'auto',
        });
      }, 50);
    }

}
