import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailInformationEventComponent } from './components/detail-information-event/detail-information-event.component';
import {DetailEventPage} from "./pages/detail-event/detail-event.page";
import {DetailEventRoutingModule} from "./detail-event-routing.module";
import {PhotoModule} from "../../features/photo/photo.module";
import {CategoriesEventModule} from "../../features/categories-event/categories-event.module";
import {CloudinaryModule} from "@cloudinary/angular-5.x";
import { CommentEventComponent } from './components/comment-event/comment-event.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatFormFieldModule} from "@angular/material/form-field";
import {RatingStartModule} from "../../features/rating-start/rating-start.module";
import {ImageGalleryModule} from "../../features/image-gallery/image-gallery.module";




@NgModule({
  declarations: [DetailInformationEventComponent, DetailEventPage, CommentEventComponent],
    imports: [
        CommonModule,
        FormsModule,
        DetailEventRoutingModule,
        PhotoModule,
        CategoriesEventModule,
        CloudinaryModule,
        ReactiveFormsModule,
        MatInputModule,
        MatFormFieldModule,
        RatingStartModule,
        ImageGalleryModule,
    ]
})
export class DetailEventModule { }
