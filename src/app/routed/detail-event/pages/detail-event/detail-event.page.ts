import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  templateUrl: './detail-event.page.html',
  styleUrls: ['./detail-event.page.css']
})

export class DetailEventPage implements OnInit {
  constructor(
    private readonly title: Title
  ) {
    this.title.setTitle('Информация о событии');
  }

  ngOnInit(): void {}

}
