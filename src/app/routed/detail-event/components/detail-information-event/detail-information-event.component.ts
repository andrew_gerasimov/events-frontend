import { Component, OnInit } from '@angular/core';
import { ExistingEvent } from '../../../../features/events/models/existing-event.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MediaArr } from '../../../../features/media-file/models/media-arr.model';
import { ExistingUser } from '../../../../features/users/models/existing-user.model';
import { DefaultOrganizerService } from '../../../../features/users/services/defaultOrganizer.service';
import { DefaultOrganizer } from '../../../../features/users/models/default-organizer.model';
import { EventCategory } from '../../../../features/other-model/category.model';
import { Title } from '@angular/platform-browser';
import { EventsService } from '../../../../features/events/services/events.service';
import { MinimalInformationUser } from '../../../../features/users/models/minimal-information-user.model';
import { CurrentUserImpl, CurrentUserService } from '../../../../core/auth/current-user.service';
import { CommentEvent } from '../../../../features/other-model/comment.model';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-detail-information-event',
  templateUrl: './detail-information-event.component.html',
  styleUrls: ['./detail-information-event.component.css']
})
export class DetailInformationEventComponent implements OnInit {
  event: ExistingEvent;
  defaultDate: string = '01.01.2021';
  currentDate = new Date();
  percentTopic: string;
  height:string;
  user: CurrentUserImpl | undefined;
  countUser: number = 0;
  countLike: number = 0;
  countFavorites: number = 0;
  subsribeEvent: boolean;
  addLike: boolean;
  addFavorite: boolean;
  medias: MediaArr | undefined;
  categories: EventCategory[];
  defaultOganizer: DefaultOrganizer;
  form: FormGroup;
  creationDate: string;
  percentChek: number;
  diffCommentDate: number;

  numberFinishDateFromEvent: number;
  numberCurrentDateFromEvent: number;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly defaultOrganizerService: DefaultOrganizerService,
    private readonly eventService: EventsService,
    private readonly currentUserService: CurrentUserService
  ) {}

  ngOnInit(): void {
    this.currentUserService.user$.subscribe((value: CurrentUserImpl) => {
      this.user = value;
    });

    this.defaultOrganizerService.getDefaultOrganizer().subscribe((data) => (this.defaultOganizer = data));
    this.route.data.subscribe((data) => {
      this.event = data.event;
      if (this.event.organizersDTO[0] !== undefined) {
        if (this.event.organizersDTO[0].medias !== undefined) this.medias = this.event.organizersDTO[0].medias;
      }
      if (this.event.categoriesEventDTOS !== undefined && this.event.categoriesEventDTOS.length !== 0) {
        this.categories = this.event.categoriesEventDTOS;
      } else {
        this.categories = [
          {
            nameCategory: 'Общая',
            color: 'bg-ec008c'
          }
        ];
      }
      if (this.event.usersDTO !== undefined && this.event.usersDTO.length !== 0) {
        this.countUser = this.event.usersDTO.length;
      }
      if (this.event.likesUser !== undefined && this.event.likesUser.length !== 0) {
        this.countLike = this.event.likesUser.length;
      }
      if (this.event.favoritesUser !== undefined && this.event.favoritesUser.length !== 0) {
        this.countFavorites = this.event.favoritesUser.length;
      }

      this.checkSubmit();
      this.checkLike();
      this.checkFavorite();
      this.diffDataComment();
    });
    // this.route.queryParams.subscribe( (params:Params) => {
    //   if (params.subsribeEvent === "true") {
    //     this.subscribeEvent()
    //   }
    //   if (params.subsribeEvent === "false") {
    //     this.unsubscribeEvent()
    //   }
    // })
    this.form = new FormGroup({
      textComment: new FormControl()
    });
    this.differenceDat();
  }

  addLikeEvent() {
    this.addLike = true;
    this.eventService.patchLike(this.event.eventId).subscribe((data) => {
      this.event = data;
    });
    this.countLike++;
  }

  addFavoriteEvent() {
    this.addFavorite = true;
    this.eventService.patchFavorite(this.event.eventId).subscribe((data) => (this.event = data));
    this.countFavorites++;
  }

  removeLikeEvent() {
    this.addLike = false;
    this.eventService.deleteLike(this.event.eventId).subscribe(() => {
      this.updateEvent();
    });
    this.countLike--;
  }

  removeFavoriteEvent() {
    this.addFavorite = false;
    this.eventService.deleteFavorite(this.event.eventId).subscribe(() => this.updateEvent());
    this.countFavorites--;
  }

  subscribeEvent() {
    this.subsribeEvent = true;
    this.eventService.patchUser(this.event.eventId).subscribe((data) => {
      this.event = data;
      this.countUser = this.event.usersDTO.length;
    });
  }

  unsubscribeEvent() {
    if (this.event.organizersDTO.length !== 0) {
      if (this.user.email !== this.event.organizersDTO[0].email) {
        this.subsribeEvent = false;
        this.eventService.deleteUser(this.event.eventId).subscribe(() => this.updateEvent());
      }
    }else {
      this.subsribeEvent = false;
      this.eventService.deleteUser(this.event.eventId).subscribe(() => this.updateEvent());
    }
  }

  private checkSubmit() {
    this.event.usersDTO.forEach((user: ExistingUser) => {
      console.log(user.userId);
      console.log(localStorage.getItem('user_id'));
      if (user.userId === localStorage.getItem('user_id')) {
        this.subsribeEvent = true;
        console.log('Нашли');
      }
    });
  }

  saveComment() {
    const comment: CommentEvent = {
      message: this.form.get('textComment').value,
      user: {
        userId: localStorage.getItem('user_id')
      }
    };
    this.eventService.patchComment(this.event.eventId, comment).subscribe((data) => {
      this.event = data;
      this.diffDataComment();
    });

    this.form.reset();
  }

  private updateEvent() {
    this.eventService.getEvent(this.event.eventId).subscribe((data) => {
      this.event = data;
      this.countUser = this.event.usersDTO.length;
    });
  }

  private differenceDat() {
    if (this.event.creation_date !== undefined && this.event.creation_date !== null) {
      this.creationDate = this.event.creation_date;
    } else {
      this.creationDate = this.defaultDate;
    }
    let startDate = new Date(this.creationDate);
    let finishDate = new Date(this.event.dateTime);
    this.numberFinishDateFromEvent = this.diffDate(startDate, finishDate);
    this.numberCurrentDateFromEvent = this.diffDate(startDate, this.currentDate);
    console.log('numberFinishDateFromEvent ' + this.numberFinishDateFromEvent);
    console.log('numberCurrentDateFromEvent ' + this.numberCurrentDateFromEvent);
    let percent = (this.numberCurrentDateFromEvent * 100) / this.numberFinishDateFromEvent;
    this.percentChek = percent > 95 ? 95 : percent;
    this.percentTopic = this.percentChek.toString() + '%';
    // this.height= this.numberFinishDateFromEvent > 100 ? '95': this.numberFinishDateFromEvent.toString()
    // this.height= this.height  + 'px'
    console.log(this.percentTopic + ' this.percentTopic');
  }

  private diffDate(d1: Date, d2: Date): number {
    let diff = Math.abs(d1.getTime() - d2.getTime());
    return Math.ceil(diff / (1000 * 3600 * 24));
  }

  private checkLike() {
    this.event.likesUser.forEach((user) => {
      if (user.userId === localStorage.getItem('user_id')) {
        this.addLike = true;
        console.log('Нашли лайки!');
      }
    });
  }

  private checkFavorite() {
    this.event.favoritesUser.forEach((user) => {
      if (user.userId === localStorage.getItem('user_id')) {
        this.addFavorite = true;
        console.log('Нашли избранное!');
      }
    });
  }
  private diffDataComment() {
    if (this.event.comments.length !== 0) {
      let commentDataLast = new Date(this.event.comments[this.event.comments.length - 1].commentDate);
      this.currentDate = new Date();
      let diff = Math.abs(commentDataLast.getTime() - this.currentDate.getTime());
      console.log('commentDate ' + commentDataLast.getHours());
      console.log('curD ' + this.currentDate.getHours());
      this.diffCommentDate = Math.ceil(diff / (1000 * 3600));
      if (this.diffCommentDate === 0) {
        this.diffCommentDate = Math.ceil(diff / (1000 * 3600 * 24 * 60));
        console.log('111' + this.diffCommentDate);
      }
      console.log('222' + this.diffCommentDate);
    } else {
      this.diffCommentDate = 1;
      console.log('333' + this.diffCommentDate);
    }
  }

  public deleteEvent() {
    this.eventService.deleteEvent(this.event.eventId).subscribe();
    this.router.navigate(['/feed'])
  }
}
