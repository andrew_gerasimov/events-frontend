import {Component, Input, OnInit} from '@angular/core';
import {CommentEvent} from "../../../../features/other-model/comment.model";
import {CurrentUserImpl, CurrentUserService} from "../../../../core/auth/current-user.service";
import {EventsService} from "../../../../features/events/services/events.service";
import {ExistingEvent} from "../../../../features/events/models/existing-event.model";

@Component({
  selector: 'app-comment-event',
  templateUrl: './comment-event.component.html',
  styleUrls: ['./comment-event.component.css']
})
export class CommentEventComponent implements OnInit {

  @Input()
  comment: CommentEvent

  @Input()
  event: ExistingEvent

  user: CurrentUserImpl | undefined;

  constructor(
    private readonly currentUserService: CurrentUserService,
    private readonly eventService: EventsService
  ) {}

  ngOnInit(): void {
    this.currentUserService.user$.subscribe((value: CurrentUserImpl) => {
      this.user = value;
    });
  }

  deleteComment(commentId:string){
    this.eventService.deleteComment(commentId).subscribe(() => this.comment= null)
  }
}
