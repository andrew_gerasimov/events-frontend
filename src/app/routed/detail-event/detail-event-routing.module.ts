import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DetailEventPage} from "./pages/detail-event/detail-event.page";
import {EventResolver} from "../../features/resolver/event.resolver";

const routes: Routes = [
  {
    path: ':id',
    component: DetailEventPage,
    resolve : {
      event: EventResolver
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailEventRoutingModule { }
