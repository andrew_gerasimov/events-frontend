import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { MainPage } from './pages/main/main.page';
import { FeedComponent } from './components/feed/feed.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { BookRatingComponent } from './components/rating/rating.component';
import { PhotoModule } from '../../features/photo/photo.module';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import { FeedRoutingModule } from './feed-routing.module';
import { CategoriesEventModule } from '../../features/categories-event/categories-event.module';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [MainPage, FeedComponent, BookRatingComponent],
  imports: [
    CommonModule,
    FeedRoutingModule,
    NgxPaginationModule,
    PhotoModule,
    CloudinaryModule,
    CategoriesEventModule,
    MatIconModule
  ],
  exports: [BookRatingComponent],
  providers: [DatePipe]
})
export class FeedModule {}
