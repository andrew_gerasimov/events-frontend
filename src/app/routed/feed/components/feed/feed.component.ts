import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../../features/form/services/common.service';
import { ExistingEvent } from '../../../../features/events/models/existing-event.model';
import { EventsService } from '../../../../features/events/services/events.service';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { query } from '@angular/animations';
import { EventCategory } from '../../../../features/other-model/category.model';
import { CurrentUserImpl, CurrentUserService } from '../../../../core/auth/current-user.service';
import { MatDialog } from '@angular/material/dialog';
import { ExistingUser } from '../../../../features/users/models/existing-user.model';
import { DefaultOrganizer } from '../../../../features/users/models/default-organizer.model';
import { DefaultOrganizerService } from '../../../../features/users/services/defaultOrganizer.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.sass']
})
export class FeedComponent implements OnInit {
  events: ExistingEvent[];
  user: CurrentUserImpl;
  date: Date;
  defaultOganizer: DefaultOrganizer;
  dateString: string;
  currentTutorial = null;
  currentIndex = -1;
  pagesCount: number;
  title = '';
  modal = false;

  dateSort = true;
  rateSort;
  membersSort;

  dateIcon = 'expand_more';
  membersIcon;
  rateIcon;

  userCity;

  loading = false;

  page = 1;
  count = 0;
  pageSize = 15;
  query = 'empty';
  categories: EventCategory[];

  // eventsArr: ExistingEvent[];
  eventsCount: bigint;
  isEmptyResponse = false;

  constructor(
    private readonly commonService: CommonService,
    private readonly eventsService: EventsService,
    public readonly datepipe: DatePipe,
    private readonly currentUserService: CurrentUserService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly defaultOrganizerService: DefaultOrganizerService,
    public dialog: MatDialog
  ) {
    this.initUser();
    this.isEmptyResponse = false;
    route.params.subscribe((val) => {
      console.log(val);
      const url = new URL(window.location.href);
      this.query = url.searchParams.get('query');
      if (!this.query) {
        this.retrieveNearestEvents();
      } else {
        switch (this.query) {
          case 'saved':
            console.log('saved');
            this.pageSize = 5;
            this.title = 'Сохраненные события';
            this.retrieveSavedEvents();
            break;
          case 'subs':
            console.log('subs');
            this.pageSize = 5;
            this.title = 'Подписки';
            this.retrieveSubscriptionEvents();
            break;
          case 'members':
            console.log('members');
            this.pageSize = 15;
            this.retrieveNearestEvents();
            break;
          case 'recs':
            console.log('recs');
            this.pageSize = 5;
            this.title = 'Ваши рекомендации';
            this.retrieveRecommendedEvents();
            break;
          default:
            this.query = 'not null';
            console.log('incorrect query');
        }
      }
    });
  }

  initUser(): void {
    this.currentUserService.user$.subscribe((value: CurrentUserImpl) => {
      this.user = value;
    });
    console.log(this.user);
  }

  ngOnInit(): void {
    this.commonService.getCategories().subscribe((response) => {
      this.categories = response;
    });

    this.router.onSameUrlNavigation = 'reload';
    this.defaultOrganizerService.getDefaultOrganizer().subscribe((data) => (this.defaultOganizer = data));

    this.initUser();
    // this.router.routeReuseStrategy.shouldReuseRoute = () => {
    //    return false;
    // };

    this.router.routeReuseStrategy.shouldReuseRoute = function (
      future: ActivatedRouteSnapshot,
      curr: ActivatedRouteSnapshot
    ) {
      if (future.url.toString() === 'feed' && curr.url.toString() === future.url.toString()) {
        return false;
      }
      return future.routeConfig === curr.routeConfig;
    };

    this.date = new Date(Date.now());
    // this.eventsService.getEventCount().subscribe((value) => {
    //   this.eventsCount = value;
    // });
    // this.retrieveNearestEvents();
  }

  handlePageChange(event): void {
    this.page = event;
    if (!this.query) {
      this.retrieveNearestEvents();
    } else {
      switch (this.query) {
        case 'saved':
          console.log('handled saved');
          this.pageSize = 5;
          // this.title = 'Сохраненные события';
          this.retrieveSavedEvents();
          break;
        case 'subs':
          console.log('handled subs');
          this.pageSize = 5;
          // this.title = 'Подписки';
          this.retrieveSubscriptionEvents();
          break;
        case 'members':
          this.pageSize = 15;
          console.log('handled default feed (nearest events)');
          this.retrieveNearestEvents();
          break;
        case 'recs':
          this.pageSize = 5;
          console.log('handled recs');
          // this.title = 'Ваши рекомендации';
          this.retrieveRecommendedEvents();
          break;
        default:
          this.query = 'not null';
          console.log('incorrect query');
      }
    }
    // this.retrieveNearestEvents();
  }

  retrieveNearestEvents(): void {
    console.log(this.user);
    if (this.user.authenticated && this.user.cityDTO) {
      this.userCity = this.user.cityDTO.cityUuidId;
    }
    // else { this.userCity = '292d40f5-7a2b-4040-a225-efbd9ed765d8'; }
    // else { this.userCity = 'ed5c3e17-05d6-4cc7-9e5a-684ad460de19'; }
    else {
      this.userCity = '';
    }
    console.log('user city for current user is ' + this.userCity);
    console.log('retrieve nearest events');
    // if (!this.user.cityDTO.titleRu) {
    //   console.log('no city');
    //   this.modal = true;
    // }
    this.date = new Date(Date.now());
    this.dateString = this.dateConvert(this.date);
    const params = this.getRequestParams(
      this.query,
      this.title,
      this.page,
      this.pageSize,
      this.dateString,
      this.userCity,
      this.dateSort,
      this.membersSort,
      this.rateSort
    );

    this.eventsService.getFiltered(params).subscribe(
      (response) => {
        if (response !== null) {
          const { events, totalItems, totalPages } = response;
          if (totalItems === 0) {
            this.isEmptyResponse = true;
          } else {
            this.isEmptyResponse = false;
            this.events = events;
          }
          this.count = totalItems;
          this.pagesCount = totalPages;
          console.log(response);
        } else {
          this.isEmptyResponse = true;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  retrieveSavedEvents(): void {
    this.events = [];

    const params = this.getSubsParams(this.page, this.pageSize, this.dateSort, this.membersSort, this.rateSort);

    this.eventsService.getFavourites(params).subscribe(
      (response) => {
        if (response !== null) {
          const { events, totalItems, totalPages } = response;
          if (totalItems === 0) {
            this.isEmptyResponse = true;
          } else {
            this.isEmptyResponse = false;
            this.events = events;
          }
          this.count = totalItems;
          this.pagesCount = totalPages;
          console.log(response);
        } else {
          this.isEmptyResponse = true;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  retrieveRecommendedEvents(): void {
    this.eventsService.getRecommendations().subscribe(
        (response) => {
          if (response) {
            // this.events = response.slice(0, 15);
            this.events = response;
            console.log(response);
          } else {
            this.isEmptyResponse = true;
          }
        },
        (error) => {
          console.log(error);
        }
    );
  }

  retrieveSubscriptionEvents(): void {
    this.events = [];

    const params = this.getSubsParams(this.page, this.pageSize, this.dateSort, this.membersSort, this.rateSort);
    this.eventsService.getSubscriptions(params).subscribe(
      (response) => {
        if (response !== null) {
          const { events, totalItems, totalPages } = response;
          this.events = events;
          this.count = totalItems;
          this.pagesCount = totalPages;
          console.log(response);
        } else {
          this.isEmptyResponse = true;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getRequestParams(searchString, title, page, pageSize, date, cityUuid, dateSort, membersSort, ratingSort): object {
    const params = {};

    console.log('date sort ', dateSort, ' members ', membersSort, ' rate ', ratingSort);

    if (searchString !== null) {
      params[`query`] = searchString;
    } else {
      params[`query`] = '';
    }

    if (date) {
      params[`dateLaterThan`] = date;
    }

    if (dateSort != undefined) {
      params[`dateSort`] = dateSort;
    }

    if (membersSort != undefined) {
      params[`memberSort`] = membersSort;
    }

    if (ratingSort != undefined) {
      params[`ratingSort`] = ratingSort;
    }

    if (cityUuid) {
      params[`cityUuid`] = cityUuid;
    }

    if (title) {
      params[`title`] = title;
    }

    // if (page) {
    params[`page`] = page - 1;
    // }

    if (pageSize) {
      params[`size`] = pageSize;
    }

    return params;
  }

  getSubsParams(page, pageSize, dateSort, membersSort, ratingSort): object {
    const params = {};

    // if (page) {
    params[`page`] = page - 1;
    // }

    if (pageSize) {
      params[`size`] = pageSize;
    }

    if (dateSort != undefined) {
      params[`dateSort`] = dateSort;
    }

    if (membersSort != undefined) {
      params[`memberSort`] = membersSort;
    }

    if (ratingSort != undefined) {
      params[`ratingSort`] = ratingSort;
    }

    return params;
  }

  private dateConvert(date: Date): string {
    return this.datepipe.transform(date, 'yyyy-MM-dd HH:mm');
  }

  handleCategoryClick(item: EventCategory): void {
    this.router.navigate(['/search'], { queryParams: { category: item.nameCategory, query: '' } });
  }

  subsSort(): void {
    if (this.membersIcon === 'expand_more') {
      this.membersIcon = 'expand_less';
    } else if (this.membersIcon === 'expand_less') {
      this.membersIcon = '';
    } else {
      this.membersIcon = 'expand_more';
    }
    console.log('subs click');
    if (this.membersSort === true) {
      this.membersSort = false;
    } else if (this.membersSort === false) {
      this.membersSort = undefined;
    } else {
      this.membersSort = true;
    }
    switch (this.query) {
      case 'saved':
        this.retrieveSavedEvents();
        break;
      case 'subs':
        this.retrieveSubscriptionEvents();
        break;
      case 'recs':
        this.retrieveRecommendedEvents();
        break;
      default:
        this.pageSize = 15;
        this.retrieveNearestEvents();
    }
  }

  ratingSort(): void {
    if (this.rateIcon === 'expand_more') {
      this.rateIcon = 'expand_less';
    } else if (this.rateIcon === 'expand_less') {
      this.rateIcon = '';
    } else {
      this.rateIcon = 'expand_more';
    }
    console.log('rate click');
    if (this.rateSort === true) {
      this.rateSort = false;
    } else if (this.rateSort === false) {
      this.rateSort = undefined;
    } else {
      this.rateSort = true;
    }
    switch (this.query) {
      case 'saved':
        this.retrieveSavedEvents();
        break;
      case 'subs':
        console.log('handled subs');
        this.retrieveSubscriptionEvents();
        break;
      case 'recs':
        this.retrieveRecommendedEvents();
        break;
      default:
        this.pageSize = 15;
        this.retrieveNearestEvents();
    }
  }

  dataSort(): void {
    if (this.dateIcon === 'expand_more') {
      this.dateIcon = 'expand_less';
    } else if (this.dateIcon === 'expand_less') {
      this.dateIcon = '';
    } else {
      this.dateIcon = 'expand_more';
    }
    console.log('date click');
    if (this.dateSort === true) {
      this.dateSort = false;
    } else if (this.dateSort === false) {
      this.dateSort = undefined;
    } else {
      this.dateSort = true;
    }
    switch (this.query) {
      case 'saved':
        this.retrieveSavedEvents();
        break;
      case 'subs':
        this.retrieveSubscriptionEvents();
        break;
      case 'recs':
        this.retrieveRecommendedEvents();
        break;
      default:
        this.pageSize = 15;
        this.retrieveNearestEvents();
    }
  }
}
