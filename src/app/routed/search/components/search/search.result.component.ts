import { Component, DoCheck, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from '../../../../features/events/services/events.service';
import { ExistingEvent } from '../../../../features/events/models/existing-event.model';
import { EventCategory } from '../../../../features/other-model/category.model';
import { CurrentUserImpl, CurrentUserService } from '../../../../core/auth/current-user.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { CityModel } from '../../../../features/other-model/city.model';
import { map, startWith } from 'rxjs/operators';
import { CommonService } from '../../../../features/form/services/common.service';
import { CountryModel } from '../../../../features/other-model/country.model';
import { DefaultOrganizer } from '../../../../features/users/models/default-organizer.model';
import { DefaultOrganizerService } from '../../../../features/users/services/defaultOrganizer.service';
import { DatePipe } from '@angular/common';
import { Options } from '@angular-slider/ngx-slider';

interface FormData {
  chipsControl: string[];
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  country: string;
  informationAboutYourself: string;
  // dateTime: number;
}

@Component({
  selector: 'app-search-result',
  templateUrl: './search.result.component.html',
  styleUrls: ['./search.result.component.css']
})
export class SearchComponent implements OnInit, DoCheck {
  events: ExistingEvent[];
  form: FormGroup;
  date: Date;
  user: CurrentUserImpl;
  defaultOganizer: DefaultOrganizer;

  minDate = new Date(Date.now());

  dateSort = true;
  rateSort;
  membersSort;

  dateIcon = 'expand_more';
  membersIcon;
  rateIcon;

  userCity;
  userCountry;

  cityUuid: string;
  query = '';
  categoryUrl = '';
  // pagesCount: number;
  page = 1;
  count = 0;
  pageSize = 15;
  isEmptyResponse = false;

  rateHigherThan;
  rateLowerThan;
  membersHigherThan;
  membersLowerThan;
  dateEarlierThan;
  dateLaterThan;
  categoryName = '';

  error = false;
  categoryIs = false;
  numberOfChanges = 0;
  defaultLengthControlCity = 0;

  cities: CityModel[];
  countries: CountryModel[];
  categories: EventCategory[];

  categoryList: string[] = [];

  chipsControlValue$: Observable<any>;

  options1: Options = {
    floor: 0,
    ceil: 100,
    step: 5
  };

  options2: Options = {
    floor: 0,
    ceil: 5,
    step: 1
  };

  filteredOptions: Observable<CountryModel[]>;
  filteredOptionsCity: Observable<CityModel[]>;
  categorySearch;

  constructor(
    private route: ActivatedRoute,
    private readonly eventsService: EventsService,
    private readonly currentUserService: CurrentUserService,
    public datepipe: DatePipe,
    private readonly commonService: CommonService,
    private readonly fb: FormBuilder,
    private readonly defaultOrganizerService: DefaultOrganizerService,
    private readonly router: Router
  ) {
    const url = new URL(window.location.href);

    this.isEmptyResponse = false;
    if (url.searchParams.get('query') !== '') {
      this.query = url.searchParams.get('query');
    } else {
      this.query = ' ';
    }
    console.log(this.query);

    console.log('url of category' + url.searchParams.get('category'));
    if (url.searchParams.get('category') !== null) {
      this.categoryUrl = url.searchParams.get('category');
      this.categorySearch = false;
    } else if (url.searchParams.get('category') === null) {
      console.log('category is null');
      this.categoryUrl = '';
      this.categorySearch = true;
    }

    console.log('category url is ');
    console.log(this.categoryUrl);
    this.initUser();
  }

  displayFnCity(city: CityModel): string {
    return city && city.titleRu ? city.titleRu : '';
  }

  initUser(): void {
    this.currentUserService.user$.subscribe((value: CurrentUserImpl) => {
      this.user = value;
      // this.cityUuid = this.user.cityDTO.cityUuidId;
    });
  }

  displayFn(country: CountryModel): string {
    return country && country.countryName ? country.countryName : '';
  }

  ngOnInit(): void {
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
    this.router.onSameUrlNavigation = 'reload';

    this.defaultOrganizerService.getDefaultOrganizer().subscribe((data) => (this.defaultOganizer = data));

    this.commonService.getCountries().subscribe((response) => {
      // this.countries = response.slice(1, response.length);
      this.countries = response;
      this.filteredOptions = this.form.get('country').valueChanges.pipe(
        startWith(''),
        map((value) => (typeof value === 'string' ? value : value.countryName)),
        map((countryName) => (countryName ? this._filter(countryName) : this.countries.slice()))
      );
    });

    this.form = this.initForm();

    this._updateOrGetCity(null);

    this.form.get('country').valueChanges.subscribe(() => this._updateOrGetCity());

    this.commonService.getCategories().subscribe((response) => {
      this.categories = response;
    });

    this.chipsControlValue$ = this.form.get('chipsControl').valueChanges;

    this.retrieveEvents();
  }

  ngDoCheck(): void {
    if (!this.categoryIs) {
      for (const category of this.categories) {
        this.categoryList.push(category.nameCategory);
      }
      console.log(this.categoryList);
      this.categoryIs = true;
    }
  }

  ngAfterViewInit() {
    this.form.get('city').valueChanges.subscribe(() => {
      if (
        this.numberOfChanges > 2 &&
        this.defaultLengthControlCity < this.form.get('city').value.length &&
        this.numberOfChanges % 2 === 0
      ) {
        this._updateOrGetCity(this.form.get('city').value);
      }
      this.defaultLengthControlCity = this.form.get('city').value.length;
      this.numberOfChanges++;
    });
  }

  initForm(): FormGroup {
    return this.fb.group({
      membersControl: new FormControl([0, 100]),
      ratingControl: new FormControl([0, 5]),
      query: this.fb.control(this.query),
      chipsControl: this.fb.control(''),
      country: this.fb.control(''),
      city: this.fb.control(''),
      members: this.fb.control(''),
      rating: this.fb.control(''),
      startDate: this.fb.control(''),
      finishDate: this.fb.control('')
    });
  }

  retrieveEvents(): void {
    // if (
    //   this.user.authenticated &&
    //   this.user.cityDTO &&
    //   (this.form.value.city === '' || this.form.value.city === null)
    // ) {
    //   this.userCity = this.user.cityDTO.cityUuidId; // 1
    // } else if (this.form.value.city !== '') {
    //   console.log('user city set from form ' + this.form.value.city);
    //   this.userCity = this.form.value.city.cityUuidId; // 2
    // } else {
    //   this.userCity = ''; // 3
    // }
    if (this.user.authenticated && this.form.value.country) {
      console.log('country is set ' + this.form.value.country.countryUuidId);
      this.userCountry = this.form.value.country.countryUuidId;
    }
    if (this.form.value.country && this.form.value.city){
      this.userCountry = '';
      this.userCity = this.form.value.city.cityUuidId;
    }
    if (!this.form.value.city && this.form.value.country ){
      this.userCity = '';
      this.userCountry = this.form.value.country.countryUuidId;
    }
    if (!this.form.value.country && !this.form.value.city){
      this.userCountry = '';
      this.userCity = this.form.value.city.cityUuidId;
      if (this.user.cityDTO) {
        this.userCity = this.user.cityDTO.cityUuidId;
      }
    }
    // console.log('get request params query is '+this.query);
    // if (this.form.value.city) this.userCity = this.form.value.city;
    const params = this.getRequestParams(
      this.page,
      this.pageSize,
      this.userCity,
      this.userCountry,
      this.query,
      this.dateSort,
      this.membersSort,
      this.rateSort,
      this.rateHigherThan,
      this.rateLowerThan,
      this.membersHigherThan,
      this.membersLowerThan,
      this.dateLaterThan,
      this.dateEarlierThan,
      this.categoryName
    );
    // this.events = [];
    // const params = this.getRequestParams(this.page, this.pageSize, '292d40f5-7a2b-4040-a225-efbd9ed765d8', this.query, true);

    this.eventsService.getFiltered(params).subscribe(
      (response) => {
        if (response !== null) {
          const { events, totalItems } = response;
          if (totalItems === 0) {
            this.isEmptyResponse = true;
          } else {
            this.isEmptyResponse = false;
            this.events = events;
          }
          this.count = totalItems;
          console.log(response);
        } else {
          this.isEmptyResponse = true;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getRequestParams(
    page,
    pageSize,
    cityUuid,
    countryUuid,
    query,
    dateSort,
    membersSort,
    ratingSort,
    rateHigherThan,
    rateLowerThan,
    membersHigherThan,
    membersLowerThan,
    dateEarlierThan,
    dateLaterThan,
    categories
  ): object {
    const params = {};

    console.log('categories here ' + categories);
    if (categories != undefined) {
      // console.log('categories to parse are: ' + categories);
      var categoriesList = '';
      for (const c of categories) {
        categoriesList += c + ',';
      }
      // categories.forEach((category) => (categoriesList += category + ','));

      // if (categoriesList) {
      categoriesList = categoriesList.slice(0, categoriesList.length - 1);
      console.log('categories list ' + categoriesList);
      params[`categories`] = categoriesList;
      // }
    }
    if (this.categoryUrl) {
      params[`categories`] = this.categoryUrl;
    }

    if (dateEarlierThan != undefined || dateEarlierThan != undefined) {
      const date1 = this.datepipe.transform(dateEarlierThan, 'yyyy-MM-dd HH:mm');
      if (date1 != undefined) {
        params[`dateEarlierThan`] = date1;
      }
    }

    if (dateLaterThan != undefined || dateLaterThan != undefined) {
      const date2 = this.datepipe.transform(dateLaterThan, 'yyyy-MM-dd HH:mm');
      if (date2 != undefined) {
        params[`dateLaterThan`] = date2;
      }
    }

    if (rateHigherThan != undefined) {
      params[`ratingHigherThan`] = rateHigherThan;
    }

    if (rateLowerThan != undefined) {
      params[`ratingLowerThan`] = rateLowerThan;
    }

    if (membersHigherThan != undefined) {
      params[`membersHigherThan`] = membersHigherThan;
    }

    if (membersLowerThan != undefined) {
      params[`membersLowerThan`] = membersLowerThan;
    }

    if (dateSort != undefined) {
      params[`dateSort`] = dateSort;
    }

    if (membersSort != undefined) {
      params[`memberSort`] = membersSort;
    }

    if (ratingSort != undefined) {
      params[`ratingSort`] = ratingSort;
    }

    if (cityUuid) {
      params[`cityUuid`] = cityUuid;
    }

    if (countryUuid) {
      // params[`cityUuid`] = '';
      params[`countryUuid`] = countryUuid;
    }

    // if (page) {
    params[`page`] = page - 1;
    // }

    if (pageSize) {
      params[`size`] = pageSize;
    }

    if (cityUuid) {
      params[`cityUuid`] = cityUuid;
    }

    // if (query) {
    if (query === null || query == '') query = ' ';
    params[`query`] = query;
    // }

    return params;
  }

  private _updateOrGetCity(value?: string): void {
    if (value === '') {
      value = null;
    }
    let country;
    try {
      // console.log('country' + this.form.get('country').value.countryUuidId);
      if (this.form.get('country').value) {
        country = this.form.get('country').value.countryUuidId;
      }
    } catch (e) {
      console.log(e);
    }
    this.commonService.getCities(country, value).subscribe((response) => {
      this.cities = response;
      this.filteredOptionsCity = this.form.get('city').valueChanges.pipe(
        startWith(''),
        map((value) => (typeof value === 'string' ? value : value.titleRu)),
        map((titleRu) => (titleRu ? this._filterCity(titleRu) : this.cities.slice()))
      );
    });
  }

  private _filter(name: string): CountryModel[] {
    const filterValue = name.toLowerCase();
    return this.countries.filter((option) => option.countryName.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filterCity(name: string): CityModel[] {
    const filterValue = name.toLowerCase();
    return this.cities.filter((option) => option.titleRu.toLowerCase().indexOf(filterValue) === 0);
  }

  handleCategoryClick(item: EventCategory): void {
    this.router.navigate(['/search'], { queryParams: { category: item.nameCategory, query: '' } });
  }

  handlePageChange(event): void {
    this.page = event;
    this.retrieveEvents();
  }

  handleFormSubmit(): void {
    const value = this.form.value;

    console.log(value);
    console.log('query');
    console.log(value.query);
    // if (this.form.controls.)

    // if (value.query == '' || value.query === null) {
    //   this.query = ' ';
    // } else {
    //   this.query = value.query;
    // }
    if (value.chipsControl !== null) {
      this.categoryName = value.chipsControl;
    }

    if (value.startDate !== null) {
      this.dateEarlierThan = value.startDate;
    }
    if (value.finishDate !== null) {
      this.dateLaterThan = value.finishDate;
    }

    if (value.ratingControl) {
      this.rateHigherThan = value.ratingControl[0];
      this.rateLowerThan = value.ratingControl[1];
    } else {
      this.rateHigherThan = 0;
      this.rateLowerThan = 5;
    }

    if (value.membersControl) {
      this.membersHigherThan = value.membersControl[0];
      this.membersLowerThan = value.membersControl[1];
    } else {
      this.membersHigherThan = 0;
      this.membersLowerThan = 100;
    }

    this.retrieveEvents();
  }

  resetMembersSlider(): void {
    this.form.reset({ membersControl: [0, 100] });
  }

  // resetRatingSlider(): void {
  //   this.form.reset({ ratingControl: [0, 5] });
  // }

  subsSort(): void {
    if (this.membersIcon === 'expand_more') {
      this.membersIcon = 'expand_less';
    } else if (this.membersIcon === 'expand_less') {
      this.membersIcon = '';
    } else {
      this.membersIcon = 'expand_more';
    }
    console.log('subs click');
    if (this.membersSort === true) {
      this.membersSort = false;
    } else if (this.membersSort === false) {
      this.membersSort = undefined;
    } else {
      this.membersSort = true;
    }
    this.retrieveEvents();
  }

  ratingSort(): void {
    if (this.rateIcon === 'expand_more') {
      this.rateIcon = 'expand_less';
    } else if (this.rateIcon === 'expand_less') {
      this.rateIcon = '';
    } else {
      this.rateIcon = 'expand_more';
    }
    console.log('rate click');
    if (this.rateSort === true) {
      this.rateSort = false;
    } else if (this.rateSort === false) {
      this.rateSort = undefined;
    } else {
      this.rateSort = true;
    }
    this.retrieveEvents();
  }

  dataSort(): void {
    if (this.dateIcon === 'expand_more') {
      this.dateIcon = 'expand_less';
    } else if (this.dateIcon === 'expand_less') {
      this.dateIcon = '';
    } else {
      this.dateIcon = 'expand_more';
    }
    console.log('date click');
    if (this.dateSort === true) {
      this.dateSort = false;
    } else if (this.dateSort === false) {
      this.dateSort = undefined;
    } else {
      this.dateSort = true;
    }
    this.retrieveEvents();
  }


  handleEmptyInput() {
    console.log('detect input change');
    if (this.form.value.query == '' || this.form.value.query === null) {
      this.query = ' ';
    } else {
      this.query = this.form.value.query;
    }
  }
}
