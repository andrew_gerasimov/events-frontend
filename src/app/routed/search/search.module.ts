import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchPage } from './pages/search/search.page';
import { SearchComponent } from './components/search/search.result.component';
import { FeedRoutingModule } from '../feed/feed-routing.module';
import { SearchRoutingModule } from './search-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { PhotoModule } from '../../features/photo/photo.module';
import { FeedModule } from '../feed/feed.module';
import { CategoriesEventModule } from '../../features/categories-event/categories-event.module';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';
import {NgxSliderModule} from "@angular-slider/ngx-slider";

@NgModule({
  declarations: [SearchPage, SearchComponent],
  imports: [
    CommonModule,
    SearchRoutingModule,
    NgxPaginationModule,
    PhotoModule,
    FeedModule,
    CategoriesEventModule,
    MatCardModule,
    MatFormFieldModule,
    MatDatepickerModule,
    NgxMaterialTimepickerModule,
    MatIconModule,
    ReactiveFormsModule,
    MatInputModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatRadioModule,
    NgxSliderModule
  ]
})
export class SearchModule {}
