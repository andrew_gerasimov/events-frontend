import { Component, OnInit } from '@angular/core';
import { EventsService } from '../../../../features/events/services/events.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.sass']
})
export class SearchPage implements OnInit {
  error?: string = undefined;

  constructor(private readonly eventsService: EventsService) {}

  ngOnInit(): void {
  }
}
