import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventsPage } from './pages/events/events.page';
import { AddEventPage } from './pages/add-event/add-event.page';
import { AuthGuards } from '../../auth.guards';
import {AddEventsGuards} from "./add-events.guards";

const routes: Routes = [
  {
    path: '',
    component: EventsPage,
    canActivate: [AuthGuards]
  },
  {
    path: 'add-event',
    component: AddEventPage,
    canActivate: [AuthGuards, AddEventsGuards],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventsRoutingModule {}
