import { AfterViewInit, Component, DoCheck, OnInit } from '@angular/core';
import { CurrentUserImpl, CurrentUserService } from '../../../../core/auth/current-user.service';
import { AbstractControl, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { first, map, startWith } from 'rxjs/operators';
import { CommonService } from '../../../../features/form/services/common.service';
import { CountryModel } from '../../../../features/other-model/country.model';
import { EventCategory } from '../../../../features/other-model/category.model';
import { EventsService } from '../../../../features/events/services/events.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
import { CityModel } from '../../../../features/other-model/city.model';
import {Router} from "@angular/router";
import {avatar_person} from "../../../../features/cloudinary/file.module";

export interface Meta {
  resource_type?: string;
  format?: string;
  url?: string;
  secure_url?: string;
  public_id?: string;
  width?: number;
  height?: number;
}
export interface Info {
  event: string;
  info: Meta;
  type?: string;
}
interface FormData {
  chipsControl: string[];
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  country: string;
  informationAboutYourself: string;
  // dateTime: number;
}

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.page.html',
  styleUrls: ['./add-event.page.css']
})
export class AddEventPage implements OnInit, DoCheck, AfterViewInit {
  user: CurrentUserImpl;
  form: FormGroup;
  imageUpload: boolean = false
  minDate = new Date(Date.now());
  error = false;
  categoryIs = false;
  numberOfChanges = 0;
  defaultLengthControlCity = 0;
  info: Info;
  meta: Meta;

  cities: CityModel[];
  categories: EventCategory[];

  categoryList: string[] = [];

  chipsControlValue$: Observable<any>;

  filteredOptionsCity: Observable<CityModel[]>;
  private widget: any = null;
  public_id: string = null;
  avatar_person;
  height;
  width;
  secure_url;




  constructor(
    private readonly getCurrentUser: CurrentUserService,
    private readonly currentUserService: CurrentUserService,
    private readonly commonService: CommonService,
    private readonly eventService: EventsService,
    private readonly title: Title,
    private readonly router: Router,
    private readonly fb: FormBuilder
  ) {
    this.title.setTitle('Создать события');
  }


  displayFnCity(city: CityModel): string {
    return city && city.titleRu ? city.titleRu : '';
  }

  ngOnInit(): void {
    this.currentUserService.user$.subscribe((value: CurrentUserImpl) => {
      this.user = value;
      console.log('value for user set');
    });
    this._updateOrGetCity(null);
    this.commonService.getCategories().subscribe((response) => {
      this.categories = response;
    });

    this.widget = (window as any).cloudinary.createUploadWidget(
      {
        /* Настройки cloudinary */
        cloudName: 'db9oarnya',
        uploadPreset: 'd2vbtsuf',
        multiple: false,
        cropping: true,
        tags: [this.user.email],
        folder: `users/${this.user.email}`,
        clientAllowedFormats: ['png', 'jpg', 'jpeg'],
        showPoweredBy: false,
        language: 'ru',
        theme: 'minimal'
      },
      (error, result) => {
        if (error !== undefined) {
          console.log(error);
        }
        this.uploadAvatarPerson(result);
      }
    );

    this.form = this.initForm();

    this.chipsControlValue$ = this.form.get('chipsControl').valueChanges;

    console.log('form initialized');

    // console.log('initialized onInit with:');
    // console.log(localStorage.getItem('auth_token'));
  }

  ngDoCheck(): void {
    if (!this.categoryIs) {
      for (const category of this.categories) {
        this.categoryList.push(category.nameCategory);
      }
      console.log(this.categoryList);
      this.categoryIs = true;
    }
  }

  initForm(): FormGroup {
    return this.fb.group({
      name: this.fb.control(''),
      place: this.fb.control(''),
      description: this.fb.control(''),
      chipsControl: this.fb.control(''),
      // country: this.fb.control(this.user.country.countryName),
      city: this.fb.control(''),
      // categories: this.categories,
      date: this.fb.control(''),
      time: this.fb.control(''),
      dateTime: this.fb.control('')
    });
  }

  handleFormSubmit(): void {
    const value = this.form.value;

    console.log(value);
    console.log(this.form.value.chipsControl);

    this.form.patchValue({
      dateTime: this.convertToDate(this.form.value.time, this.form.value.date)
    });
    // TODO add files
    this.eventService
      .createEvent(value.date, value.place, value.name, value.description, value.city.cityUuidId, value.chipsControl,
        this.public_id, this.avatar_person, this.height, this.width, this.secure_url)
      .subscribe(
        (data ) => {
          console.log('event creation success');
          this.router.navigate(['/event', data.eventId]);
        },
        (error: HttpErrorResponse) => {
          console.log('event creation error');
          console.log(error);
        }
      );
    console.log(this.form.value);
  }

  private _filterCity(name: string): CityModel[] {
    const filterValue = name.toLowerCase();
    return this.cities.filter((option) => option.titleRu.toLowerCase().indexOf(filterValue) === 0);
  }

  private _updateOrGetCity(value: string): void {
    if (value === '') {
      value = null;
    }
    this.commonService.getCities(this.user.country.countryUuidId, value).subscribe((response) => {
      this.cities = response;
      this.filteredOptionsCity = this.form.get('city').valueChanges.pipe(
        startWith(''),
        map((value) => (typeof value === 'string' ? value : value.titleRu)),
        map((titleRu) => (titleRu ? this._filterCity(titleRu) : this.cities.slice()))
      );
    });
  }

  convertToDate(time: string, date: any): Date {
    const timeArr = time.split(':');
    date.setSeconds(0);
    date.setMinutes(timeArr[1]);
    date.setHours(timeArr[0]);
    return date;
  }

  ngAfterViewInit(): void{
    this.form.get('city').valueChanges.subscribe(() => {
      if (this.numberOfChanges > 2 &&
        this.defaultLengthControlCity < this.form.get('city').value.length &&
        this.numberOfChanges%2 === 0)
      {
        this._updateOrGetCity(this.form.get('city').value);
      }
      this.defaultLengthControlCity = this.form.get('city').value.length
      this.numberOfChanges++;
    });
  }

  onOpenUpload($event): void {
    this.widget.open();
    console.log('Open upload button is clicked!', $event);
  }

  uploadAvatarPerson(result: any): void {
    this.info = result;
    if (this.info.event === 'success') {
      this.public_id = this.info.info.public_id;
      this.avatar_person = avatar_person;
      this.width = this.info.info.width;
      this.height = this.info.info.height;
      this.secure_url = this.info.info.secure_url;
      this.imageUpload = true
    }
  }
}
