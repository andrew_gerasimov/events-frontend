import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.css']
})
export class EventsPage implements OnInit {
  constructor(private readonly router: Router, private readonly title: Title) {
    this.title.setTitle('Мои события');
  }

  ngOnInit(): void {}

  handleEventCreate(): void {
    this.router.navigate([`/events/add-event`]).then(() => this.router.onSameUrlNavigation);
  }
}
