import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {ExistingEvent} from "../events/models/existing-event.model";
import {EventsService} from "../events/services/events.service";
import {Observable} from "rxjs";
import {ChatRoomInterfaces} from "../../routed/chat/models/chatRoom.interfaces";
import {ChatService} from "../../routed/chat/service/chat.service";
import {ChatMessageInterfaces} from "../../routed/chat/models/chatMessage.interfaces";

@Injectable({ providedIn: "root"})
export class ChatRoomResolver implements Resolve<ChatMessageInterfaces[]>{
  constructor(private readonly chatService: ChatService) {}
  resolve(route: ActivatedRouteSnapshot): Observable<ChatMessageInterfaces[]> | Promise<ChatMessageInterfaces[]> | ChatMessageInterfaces[] {
    return this.chatService.getChatMessages(route.params['id']);
  }
}
