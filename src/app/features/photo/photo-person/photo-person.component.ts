import { AfterContentInit, ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { MediaArr } from '../../media-file/models/media-arr.model';
import { avatar_person, photo_person } from '../../cloudinary/file.module';
import {CurrentUserImpl, CurrentUserService} from "../../../core/auth/current-user.service";

@Component({
  selector: 'app-photo-person',
  templateUrl: './photo-person.component.html',
  styleUrls: ['./photo-person.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class PhotoPersonComponent implements OnInit {
  _medias: MediaArr;

  @Input() set medias(value: MediaArr) {
    this._medias = value;
    this.getAvatarPhoto();
  }

  @Input()
  prem?:boolean

  get medias(): MediaArr {
    return this._medias;
  }

  photoAvatarId: string;
  defaultPhotoId: string;

  constructor(
    private readonly currentUserService: CurrentUserService
  ) {}

  ngOnInit(): void {
    this.getAvatarPhoto();
  }

  private getAvatarPhoto(): void {
    let photo_media = this.medias;
    for (let photoMediaKey in photo_media) {
      try {
        // первый объект медиа с категорией аватарка будет выведен,
        // так как в базе мы сортируем по дате создания
        // у нас всегда есть фотография, и они отсортированы по дате добавления
        if (photo_media[photoMediaKey].categories.categoryFileId === avatar_person) {
          this.photoAvatarId = photo_media[photoMediaKey].mediaUrl;
          break;
        }
        if (photo_media[photoMediaKey].categories.categoryFileId === photo_person) {
          this.defaultPhotoId = photo_media[photoMediaKey].mediaUrl;
          break;
        }
      } catch (e) {
        console.log(e);
      }
    }
  }
}
