import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotoPersonComponent } from './photo-person/photo-person.component';
import {CloudinaryModule} from "@cloudinary/angular-5.x";



@NgModule({
  declarations: [PhotoPersonComponent],
  exports: [
    PhotoPersonComponent
  ],
  imports: [
    CommonModule,
    CloudinaryModule
  ]
})
export class PhotoModule { }
