import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleGalleryComponent } from './simple-gallery/simple-gallery.component';
import {CloudinaryModule} from "@cloudinary/angular-5.x";
import {RouterModule} from "@angular/router";
import {PhotoModule} from "../photo/photo.module";



@NgModule({
  declarations: [SimpleGalleryComponent],
  exports: [
    SimpleGalleryComponent
  ],
  imports: [
    CommonModule,
    CloudinaryModule,
    RouterModule,
    PhotoModule
  ]
})
export class ImageGalleryModule { }
