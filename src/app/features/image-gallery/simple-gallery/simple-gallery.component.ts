import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {FileMedia} from "../../media-file/models/file-media.model";
import {MediaArr} from "../../media-file/models/media-arr.model";
import {UserService} from "../../users/services/users.service";
import {RefreshUserService} from "../../../core/auth/refresh-user.service";
import {Router} from "@angular/router";
import {avatar_person} from "../../cloudinary/file.module";
import {HttpErrorResponse} from "@angular/common/http";
import {CurrentUserImpl, CurrentUserService} from "../../../core/auth/current-user.service";
import {AnotherUser} from "../../users/models/another-user.model";

@Component({
  selector: 'app-simple-gallery',
  templateUrl: './simple-gallery.component.html',
  styleUrls: ['./simple-gallery.component.css'],
})
export class SimpleGalleryComponent implements OnInit {

  @Input()
  images: FileMedia[];
  @Input()
  us: AnotherUser;
  userCurrentSession: CurrentUserImpl;

  constructor( private readonly getUserService: RefreshUserService,
               private readonly userService: UserService,
               private readonly rout: Router,
               private readonly currentUserService: CurrentUserService
) { }

  ngOnInit(): void {
    this.currentUserService.user$.subscribe((value: CurrentUserImpl) => {
      this.userCurrentSession = value;
    });
  }

  deleteImage(pk: string) {
    console.log(pk)
    this.userService.deletedMedia(pk).subscribe(
      () => {
        //      console.log('фотография удалена!')
        this.getUserService.refreshCurrentUser().toPromise();
        console.log(this.images.length)
        if (this.images.length === 2)
          //надо в компоненте, который отвечает за аватарку посмотреть проверку на тип
          window.location.reload()
      }
    );
  }
}
