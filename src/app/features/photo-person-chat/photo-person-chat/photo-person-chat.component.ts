import {Component, Input, OnInit} from '@angular/core';
import {FileMedia} from "../../media-file/models/file-media.model";
import {MediaArr} from "../../media-file/models/media-arr.model";
import {CurrentUserService} from "../../../core/auth/current-user.service";
import {avatar_person, photo_person} from "../../cloudinary/file.module";

@Component({
  selector: 'app-photo-person-chat',
  templateUrl: './photo-person-chat.component.html',
  styleUrls: ['./photo-person-chat.component.css']
})
export class PhotoPersonChatComponent implements OnInit {
  @Input()
  media: FileMedia

  photoAvatarId: string;
  defaultPhotoId: string;

  @Input()
  prem?:boolean
  constructor(
  ) {}

  ngOnInit(): void {
    if (this.media.categories.categoryFileId === avatar_person) {
      this.photoAvatarId = this.media.mediaUrl;
    }
    if (this.media.categories.categoryFileId === photo_person) {
      this.defaultPhotoId = this.media.mediaUrl;
    }
  }
}
