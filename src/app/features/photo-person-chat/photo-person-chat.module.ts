import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotoPersonChatComponent } from './photo-person-chat/photo-person-chat.component';
import {CloudinaryModule} from "@cloudinary/angular-5.x";



@NgModule({
  declarations: [PhotoPersonChatComponent],
  exports: [
    PhotoPersonChatComponent
  ],
  imports: [
    CommonModule,
    CloudinaryModule
  ]
})
export class PhotoPersonChatModule { }
