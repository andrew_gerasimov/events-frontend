import { CategoryArr } from '../../other-model/CategoryArr.model';
import { UserArrModel } from '../../other-model/UserArr.model';
import { CurrentUserImpl } from '../../../core/auth/current-user.service';
import { EventCategory } from '../../other-model/category.model';
import { OrganizersParticipantsArrayModel } from '../../other-model/organizersParticipantsArrayModel';
import { CityModel } from '../../other-model/city.model';
import {FileMedia} from "../../media-file/models/file-media.model";

export interface NewEvent {
  dateTime: string;
  place: string;
  briefDescription: string;
  fullDescription: string;
  cityDTO: CityModel;
  categoriesEventDTOS: EventCategory[];
  organizersDTO: OrganizersParticipantsArrayModel[];
  usersDTO: OrganizersParticipantsArrayModel[];
  medias?: FileMedia[];
  securityUrl?: string;
}
