import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {EventsService} from "../../events/services/events.service";
import {ExistingEvent} from "../../events/models/existing-event.model";
import {CurrentUserImpl} from "../../../core/auth/current-user.service";

@Component({
  selector: 'app-rating-event',
  templateUrl: './rating-event.component.html',
  styleUrls: ['./rating-event.component.css']
})
export class RatingEventComponent implements OnInit {

  @Input()
  event: ExistingEvent;

  @Input()
  user: CurrentUserImpl;

  @Output()
  eventChange = new EventEmitter<ExistingEvent>();
  currentRate: number;
  canPut: boolean = true;
  lookMessage: boolean = true;

  ctrl: FormControl
  summaRating: number = 0;

  constructor(private readonly eventService: EventsService) { }

  ngOnInit(): void {
    this.ratingPreset()
    this.ctrl = new FormControl(this.currentRate, Validators.required);
    this.checkAuthorization()
    if (!this.canPut) {
      this.ctrl.disable()
    }
  }

  toggle() {
    if (this.canPut) {
      if (this.ctrl.disabled) {
        this.ctrl.enable();
      } else {
        this.ctrl.disable();
        this.patchRating()
      }
    }
  }

  private patchRating() {
    this.eventService.patchRating(this.event.eventId, {
      valRating : this.ctrl.value,
      user : {
        userId : localStorage.getItem('user_id')
      }
    }).subscribe( (data) => {
        this.event = data;
        this.eventChange.emit(this.event);
      }
    )
  }

  private ratingPreset(){
    if (this.event.ratings.length !== 0){
      this.event.ratings.forEach(rating => {
        this.summaRating += rating.valRating
        this.checkUserHasAlreadyRatedEvent(rating.user.userId)
      })
      console.log(this.summaRating)
      console.log(this.event.ratings.length)

      this.currentRate = this.summaRating/this.event.ratings.length
    }
    else {
      this.currentRate = 0
    }
  }

  private checkUserHasAlreadyRatedEvent(id: string): void{
    if (id === localStorage.getItem('user_id')) {
      this.canPut = false
      this.lookMessage = false
    }
  }

  private checkAuthorization(){
    if (!this.user.email) {
      this.canPut = false
      this.lookMessage = false
    }
  }
}
