import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RatingEventComponent } from './rating-event/rating-event.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [RatingEventComponent],
  exports: [
    RatingEventComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    ReactiveFormsModule
  ]
})
export class RatingStartModule { }
