import { EventCategory } from './category.model';

export interface CategoryArr {
  categories: EventCategory[];
}
