export interface EventCategory {
  nameCategory: string;
  color?: string;
}
