import { EventCategory } from './category.model';
import { MediaArr } from '../media-file/models/media-arr.model';

export class OrganizersParticipantsArrayModel {
  userId: string;
  email?: string;
  medias?: MediaArr;
  firstName?: string;
  lastName?: string;

  constructor(userId: string) {
    this.userId = userId;
  }
}
