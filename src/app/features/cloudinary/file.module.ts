import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import { MatButtonModule } from '@angular/material/button';
import { PhotoModule } from '../photo/photo.module';

export const avatar_person = 1;
export const photo_person = 2;
export const avatar_event = 3;
export const photo_event = 4;

@NgModule({
  declarations: [UploadFileComponent],
  exports: [UploadFileComponent],
  imports: [CommonModule, MatIconModule, RouterModule, CloudinaryModule, PhotoModule, MatButtonModule]
})
export class FileModule {}
