import { AfterContentChecked, AfterViewChecked, Component, DoCheck, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { CurrentUserImpl, CurrentUserService } from '../../../core/auth/current-user.service';
import {avatar_person, photo_person} from '../file.module';
import { RefreshUserService } from '../../../core/auth/refresh-user.service';
import {UserService} from "../../users/services/users.service";
import {MediaArr} from "../../media-file/models/media-arr.model";

export interface Meta {
  resource_type?: string;
  format?: string;
  url?: string;
  secure_url?: string;
  public_id?: string;
  width?: number;
  height?: number;
}
export interface Info {
  event: string;
  info: Meta;
  type?: string;
}

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.css']
})
export class UploadFileComponent implements OnInit {
  user: CurrentUserImpl;
  info: Info;
  meta: Meta;
  medias: MediaArr
  private widget: any = null;

  constructor(
    private http: HttpClient,
    private readonly router: Router,
    private readonly getUserService: RefreshUserService,
    private readonly userService: UserService,

    private readonly currentUserService: CurrentUserService
  ) {}

  ngOnInit(): void {
    this.currentUserService.user$.subscribe((value: CurrentUserImpl) => {
      this.user = value;
      this.medias = this.user.medias
    });
    this.widget = (window as any).cloudinary.createUploadWidget(
      {
        /* Настройки cloudinary */
        cloudName: 'db9oarnya',
        uploadPreset: 'd2vbtsuf',
        multiple: false,
        cropping: true,
        tags: [this.user.email],
        folder: `users/${this.user.email}`,
        clientAllowedFormats: ['png', 'jpg', 'jpeg'],
        showPoweredBy: false,
        language: 'ru',
        theme: 'minimal'
      },
      (error, result) => {
        if (error !== undefined) {
          console.log(error);
        }
        this.uploadAvatarPerson(result);
      }
    );
  }

  onOpenUpload($event): void {
    this.widget.open();
    console.log('Open upload button is clicked!', $event);
  }

  uploadAvatarPerson(result: any): void {
    this.info = result;
    if (this.info.event === 'success') {
      this.userService.uploadFile(this.info.info.public_id, avatar_person, this.info.info.width, this.info.info.height,
        this.info.info.secure_url).subscribe(
        () => {
          this.getUserService.refreshCurrentUser().toPromise();
          //this.medias = this.user.medias
        },
        (error: HttpErrorResponse) => {
          console.log('error');
          console.error('Error', error);
        }
      );
    }
  }
}
