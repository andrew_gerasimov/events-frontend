import { Component, Input, OnInit } from '@angular/core';
import { EventCategory } from '../../other-model/category.model';

@Component({
  selector: 'app-tag-event',
  templateUrl: './tag-event.component.html',
  styleUrls: ['./tag-event.component.css']
})
export class TagEventComponent implements OnInit {
  @Input()
  category: EventCategory;
  constructor() {}

  ngOnInit(): void {}
}
