import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatChip, MatChipList } from '@angular/material/chips';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { map } from 'rxjs/operators';
import { MediaArr } from '../../media-file/models/media-arr.model';

@UntilDestroy()
@Component({
  selector: 'app-chips-events',
  templateUrl: './chips-events.component.html',
  styleUrls: ['./chips-events.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: ChipsEventsComponent,
      multi: true
    }
  ]
})
export class ChipsEventsComponent implements OnInit, AfterViewInit, ControlValueAccessor {
  @ViewChild(MatChipList)
  chipList!: MatChipList;

  private _options: string[];

  @Input() set options(value: string[]) {
    this._options = value;
  }

  get options(): string[] {
    return this._options;
  }

  value: string[] = [];

  onChange!: (value: string[]) => void;
  onTouch: any;

  disabled = false;

  constructor() {}

  writeValue(value: string[]): void {
    // When form value set when chips list initialized
    if (this.chipList && value) {
      this.selectChips(value);
    } else if (value) {
      // When chips not initialized
      this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  ngOnInit(): void {
    console.log('ngOnInit');
  }

  ngAfterViewInit(): void {
    this.selectChips(this.value);

    this.chipList.chipSelectionChanges
      .pipe(
        untilDestroyed(this),
        map((event) => event.source)
      )
      .subscribe((chip) => {
        if (chip.selected) {
          this.value = [...this.value, chip.value];
        } else {
          this.value = this.value.filter((o) => o !== chip.value);
        }

        this.propagateChange(this.value);
      });
  }

  propagateChange(value: string[]): void {
    if (this.onChange) {
      this.onChange(value);
    }
  }

  selectChips(value: string[]): void {
    this.chipList.chips.forEach((chip) => chip.deselect());

    const chipsToSelect = this.chipList.chips.filter((c) => value.includes(c.value));

    chipsToSelect.forEach((chip) => chip.select());
  }

  toggleSelection(chip: MatChip): void {
    if (!this.disabled) {
      chip.toggleSelected();
    }
  }
}
