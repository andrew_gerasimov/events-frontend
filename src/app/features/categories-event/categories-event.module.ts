import { NgModule } from '@angular/core';
import { ChipsEventsComponent } from './chips-events/chips-events.component';
import {CommonModule} from "@angular/common";

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { TagEventComponent } from './tag-event/tag-event.component';


@NgModule({
  declarations: [ChipsEventsComponent, TagEventComponent],
  exports: [
    ChipsEventsComponent,
    TagEventComponent
  ],
  imports: [
    CommonModule,
    MatChipsModule,
    MatIconModule,
    MatToolbarModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCheckboxModule,
  ]
})
export class CategoriesEventModule { }
