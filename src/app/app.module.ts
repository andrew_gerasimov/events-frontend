import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { CurrentUserModule } from './features/current-user/current-user.module';
import { Cloudinary, CloudinaryConfiguration, CloudinaryModule } from '@cloudinary/angular-5.x';
import cloudinaryConfiguration from './config';
import { SharedModule } from './routed/shared/shared.module';
import { Cloudinary as CloudinaryCore } from 'cloudinary-core';
import { CoreModule } from './core/core.module';
import { FileModule } from './features/cloudinary/file.module';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FeedModule } from './routed/feed/feed.module';
import { SearchModule } from './routed/search/search.module';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { DialogModule } from './routed/dialog/dialog.module';
import { DialogComponent } from './routed/dialog/dialog/dialog.component';
import { AuthGuards } from './auth.guards';
import { DialogLimitationsPremComponent } from './routed/dialog/dialog-limitations-prem/dialog-limitations-prem.component';
import { MatButtonModule } from '@angular/material/button';
import { NgxSliderModule } from '@angular-slider/ngx-slider';

export const cloudinary = {
  Cloudinary: CloudinaryCore
};

export const config: CloudinaryConfiguration = cloudinaryConfiguration;

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    NgxSliderModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SearchModule,
    SharedModule,
    HttpClientModule,
    FeedModule,
    CurrentUserModule,
    CloudinaryModule.forRoot(cloudinary, config),
    CoreModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FileModule,
    NgxMaterialTimepickerModule,
    FormsModule,
    MatInputModule,
    DialogModule,
    MatButtonModule
  ],
  entryComponents: [DialogComponent, DialogLimitationsPremComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
